<?php

namespace banco;

use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model
{
    protected $table='prestamo';

    protected $primaryKey='idprestamo';

    public $timestamps=false;


    protected $fillable =[
    	'idcliente',
    	'tipo_comprobante',
    	'serie_comprobante',
    	'num_comprobante',
    	'fecha_hora',
    	'impuesto',
    	'total_prestamo',
    	'estado'
    ];

    protected $guarded =[


    ];
}