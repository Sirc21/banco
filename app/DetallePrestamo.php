<?php

namespace banco;

use Illuminate\Database\Eloquent\Model;

class DetallePrestamo extends Model
{
     protected $table='detalle_prestamo';

    protected $primaryKey='iddetalle_prestamo';

    public $timestamps=false;


    protected $fillable =[
    	'id_prestamo',
    	'id_servicio',
    	'cantidad',
    	'precio_prestamo',
    	'descuento'
    ];

    protected $guarded =[

    ];
}