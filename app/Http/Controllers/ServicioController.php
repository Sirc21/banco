<?php

namespace banco\Http\Controllers;

use Illuminate\Http\Request;

use banco\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use banco\Http\Requests\ServicioFormRequest;
use banco\Servicio;
use DB;

class ServicioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $servicios=DB::table('servicio as a')
            ->join('categoria as c','a.idcategoria','=','c.idcategoria')
            ->select('a.idservicio','a.nombre','a.codigo','a.stock','c.nombre as categoria','a.descripcion','a.imagen','a.estado')
            ->where('a.nombre','LIKE','%'.$query.'%')
            ->orwhere('a.codigo','LIKE','%'.$query.'%')
            ->orderBy('idservicio','desc')
            ->paginate(7);
            return view('almacen.servicio.index',["servicios"=>$servicios,"searchText"=>$query]);
        }
    }
    public function create()
    {
        $categorias=DB::table('categoria')->where('condicion','=','1')->get();
        return view("almacen.servicio.create",["categorias"=>$categorias]);
    }
    public function store (ServicioFormRequest $request)
    {
        $servicio=new Servicio;
        $servicio->idcategoria=$request->get('idcategoria');
        $servicio->codigo=$request->get('codigo');
        $servicio->nombre=$request->get('nombre');
        $servicio->stock=$request->get('stock');
        $servicio->descripcion=$request->get('descripcion');
        $servicio->estado='Activo';

        if (Input::hasFile('imagen')){
        	$file=Input::file('imagen');
        	$file->move(public_path().'/imagenes/servicios/',$file->getClientOriginalName());
        	$servicio->imagen=$file->getClientOriginalName();
        }
        $servicio->save();
        return Redirect::to('almacen/servicio');

    }
    public function show($id)
    {
        return view("almacen.servicio.show",["servicio"=>Servicio::findOrFail($id)]);
    }
    public function edit($id)
    {
        $servicio=Servicio::findOrFail($id);
        $categorias=DB::table('categoria')->where('condicion','=','1')->get();
        return view("almacen.servicio.edit",["servicio"=>$servicio,"categorias"=>$categorias]);
    }
    public function update(ServicioFormRequest $request,$id)
    {
        $servicio=Servicio::findOrFail($id);

        $servicio->idcategoria=$request->get('idcategoria');
        $servicio->codigo=$request->get('codigo');
        $servicio->nombre=$request->get('nombre');
        $servicio->stock=$request->get('stock');
        $servicio->descripcion=$request->get('descripcion');

        if (Input::hasFile('imagen')){
        	$file=Input::file('imagen');
        	$file->move(public_path().'/imagenes/servicios/',$file->getClientOriginalName());
        	$servicio->imagen=$file->getClientOriginalName();
        }
        $servicio->update();
        return Redirect::to('almacen/servicio');
    }
    public function destroy($id)
    {
        $servicio=Servicio::findOrFail($id);
        $servicio->estado='Inactivo';
        $servicio->update();
        return Redirect::to('almacen/servicio');
    }
}
