<?php

namespace banco\Http\Controllers;

use Illuminate\Http\Request;

use banco\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use banco\Http\Requests\IngresoFormRequest;
use banco\Ingreso;
use banco\DetalleIngreso;
use DB;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class IngresoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
        	//trimp borra espacios del principio y final
            $query=trim($request->get('searchText'));
            $ingresos=DB::table('ingreso as i')
            ->join('persona as p','i.idproveedor','=','p.idpersona')
            ->join('detalle_ingreso as di','i.idingreso','=','di.idingreso')
            ->select('i.idingreso','i.fecha_hora','p.nombre','i.tipo_comprobante','i.serie_comprobante','i.numero_comprobante','i.impuesto','i.estado',DB::raw('sum(di.cantidad*precio_compra) as total'))
            ->where('i.numero_comprobante','LIKE','%'.$query.'%')
            ->orderBy('i.idingreso','desc')
            ->groupBy('i.idingreso','i.fecha_hora','p.nombre','i.tipo_comprobante','i.serie_comprobante','i.numero_comprobante','i.impuesto','i.estado')
            ->paginate(7);

            return view('compras.ingreso.index',["ingresos"=>$ingresos,"searchText"=>$query]);
        }
    }
     public function create()
    {
        $personas=DB::table('persona')->where('tipo_persona','=','Proveedor')->get();
        $servicios = DB::table('servicio as ser')
        	->select(DB::raw('CONCAT(ser.codigo, " ",ser.nombre) AS servicio'),'ser.idservicio')
        	->where('ser.estado','=','Activo')
        	->get();
        return view('compras.ingreso.create',["personas"=>$personas,"servicios"=>$servicios]);
    }

     public function store (IngresoFormRequest $request)
    {
        try{
        	DB::beginTransaction();
        	$ingreso=new Ingreso;
		    $ingreso->idproveedor=$request->get('idproveedor');
		    $ingreso->tipo_comprobante=$request->get('tipo_comprobante');
		    $ingreso->serie_comprobante=$request->get('serie_comprobante');
		    $ingreso->numero_comprobante=$request->get('numero_comprobante');
		    $mytime = Carbon::now('America/La_Paz');
		    $ingreso->fecha_hora=$mytime->toDateTimeString();
		    $ingreso->impuesto='18';
		    $ingreso->estado='A';
		    $ingreso->save();

		    $idservicio=$request->get('idservicio');
		    $cantidad=$request->get('cantidad');
		    $precio_compra=$request->get('precio_compra');
		    $precio_prestamo=$request->get('precio_prestamo');

		    $cont = 0;

		    while($cont < count($idservicio)){
		    	$detalle=new DetalleIngreso();
		   		$detalle->idingreso=$ingreso->idingreso;
		   		$detalle->idservicio=$idservicio[$cont];
		   		$detalle->cantidad=$cantidad[$cont];
		   		$detalle->precio_compra=$precio_compra[$cont];
		   		$detalle->precio_prestamo=$precio_prestamo[$cont];
		   		$detalle->save();
		   		$cont=$cont+1;
		    }

        	DB::commit();

        }catch(Exception $e)
        {
        	DB::rollback();
        }
        
        return Redirect::to('compras/ingreso');

    }

    public function show($id)
    {
    	$ingreso=DB::table('ingreso as i')
            ->join('persona as p','i.idproveedor','=','p.idpersona')
            ->join('detalle_ingreso as di','i.idingreso','=','di.idingreso')
            ->select('i.idingreso','i.fecha_hora','p.nombre','i.tipo_comprobante','i.serie_comprobante','i.numero_comprobante','i.impuesto','i.estado',DB::raw('sum(di.cantidad*precio_compra) as total'))
            ->where('i.idingreso','=',$id)
            ->first();

    	$detalles=DB::table('detalle_ingreso as d')
            ->join('servicio as s','d.idservicio','=','s.idservicio')
            ->select('s.nombre as servicio','d.cantidad','d.precio_compra','d.precio_prestamo')
            ->where('d.idingreso','=',$id)
            ->get();
        return view("compras.ingreso.show",["ingreso"=>$ingreso,"detalles"=>$detalles]);
    }

    public function destroy($id)
    {
        $ingreso=Ingreso::findOrFail($id);
        $ingreso->Estado='C';
        $ingreso->update();
        return Redirect::to('compras/ingreso');
    }
}