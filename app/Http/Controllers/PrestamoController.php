<?php

namespace banco\Http\Controllers;

use Illuminate\Http\Request;

use banco\Http\Requests;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use banco\Http\Requests\PrestamoFormRequest;
use banco\Prestamo;
use banco\DetallePrestamo;
use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;
use PDF;

class PrestamoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function pdf($id)
    {
    $prestamo=\dbbanco\Prestamo::findOrFail($id);
        $view = view('prestamo.show', compact('prestamo'));
        $pdf = \App::make(dompdf.wrapper);
        $pdf->loadHTML($view);
         return $pdf->stream('prestamo');
    }

    public function index(Request $request)
    {
        if ($request)
        {
        	//trimp borra espacios del principio y final
            $query=trim($request->get('searchText'));
            $prestamos=DB::table('prestamo as v')
            ->join('persona as p','v.idcliente','=','p.idpersona')
            ->join('detalle_prestamo as dv','v.idprestamo','=','dv.idprestamo')
            ->select('v.idprestamo','v.fecha_hora','p.nombre','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado','v.total_prestamo')
            ->where('v.num_comprobante','LIKE','%'.$query.'%')
            ->orderBy('v.idprestamo','desc')
            ->groupBy('v.idprestamo','v.fecha_hora','p.nombre','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado')
            ->paginate(7);

            return view('prestamos.prestamo.index',["prestamos"=>$prestamos,"searchText"=>$query]);
        }
    }
     public function create()
    {
        $personas=DB::table('persona')->where('tipo_persona','=','Cliente')->get();
        $servicios = DB::table('servicio as ser')
        	->join('detalle_ingreso as di','ser.idservicio','=','di.idservicio')
        	->select(DB::raw('CONCAT(ser.codigo, " ",ser.nombre) AS servicio'),'ser.idservicio','ser.stock',DB::raw('avg(di.precio_prestamo) as precio_promedio'))
        	->where('ser.estado','=','Activo')
        	->where('ser.stock','>','0')
        	->groupBy('servicio','ser.idservicio','ser.stock')
        	->get();
        return view('prestamos.prestamo.create',["personas"=>$personas,"servicios"=>$servicios]);
    }

     public function store (PrestamoFormRequest $request)
    {
        try{
        	DB::beginTransaction();
        	$prestamo=new Prestamo;
		    $prestamo->idcliente=$request->get('idcliente');
		    $prestamo->tipo_comprobante=$request->get('tipo_comprobante');
		    $prestamo->serie_comprobante=$request->get('serie_comprobante');
		    $prestamo->total_prestamo=$request->get('total_prestamo');

		    $mytime = Carbon::now('America/La_Paz');
		    $prestamo->fecha_hora=$mytime->toDateTimeString();
		    $prestamo->impuesto='18';
		    $prestamo->estado='A';
		    $prestamo->save();

		    $idservicio=$request->get('idservicio');
		    $cantidad=$request->get('cantidad');
		    $descuento=$request->get('descuento');
		    $precio_prestamo=$request->get('precio_prestamo');

		    $cont = 0;

		    while($cont < count($idservicio)){
		    	$detalle=new DetallePrestamo();
		   		$detalle->idprestamo=$prestamo->idprestamo;
		   		$detalle->idservicio=$idservicio[$cont];
		   		$detalle->cantidad=$cantidad[$cont];
		   		$detalle->descuento=$descuento[$cont];
		   		$detalle->precio_prestamo=$precio_prestamo[$cont];
		   		$detalle->save();
		   		$cont=$cont+1;
		    }

        	DB::commit();

        }catch(Exception $e)
        {
        	DB::rollback();
        }
        
        return Redirect::to('prestamos/prestamo');

    }

    public function show($id)
    {
    	$prestamo=DB::table('prestamo as v')
            ->join('persona as p','v.idcliente','=','p.idpersona')
            ->join('detalle_prestamo as dv','v.idprestamo','=','dv.idprestamo')
            ->select('v.idprestamo','v.fecha_hora','p.nombre','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado','v.total_prestamo')
            ->where('v.idprestamo','=',$id)
            ->first();

    	$detalles=DB::table('detalle_prestamo as d')
            ->join('servicio as s','d.idservicio','=','s.idservicio')
            ->select('s.nombre as servicio','d.cantidad','d.descuento','d.precio_prestamo')
            ->where('d.idprestamo','=',$id)
            ->get();

        return view("prestamos.prestamo.show",["prestamo"=>$prestamo,"detalles"=>$detalles]);

    }

    public function destroy($id)
    {
        $prestamo=Prestamo::findOrFail($id);
        $prestamo->Estado='C'; //cancelar
        $prestamo->update();
        return Redirect::to('prestamos/prestamo');
    }
}
