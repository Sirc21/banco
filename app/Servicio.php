<?php

namespace banco;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table='servicio';

    protected $primaryKey='idservicio';

    public $timestamps=false;


    protected $fillable =[
    	'idcategoria',
    	'codigo',
    	'nombre',
    	'stock',
    	'descripcion',
    	'imagen',
    	'estado'
    ];

    protected $guarded =[


    ];
}
