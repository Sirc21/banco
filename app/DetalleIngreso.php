<?php

namespace banco;

use Illuminate\Database\Eloquent\Model;

class DetalleIngreso extends Model
{
    protected $table='detalle_ingreso';

    protected $primaryKey='iddetalle_ingreso';

    public $timestamps=false;


    protected $fillable =[
       	'idingreso',
    	'idservicio',
    	'cantidad',
    	'precio_compra',
    	'precio_prestamo'
    ];

    protected $guarded =[


    ];
}