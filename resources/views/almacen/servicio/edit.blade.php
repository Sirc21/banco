@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Servicio: {{ $servicio->nombre}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>


			{!!Form::model($servicio,['method'=>'PATCH','route'=>['almacen.servicio.update',$servicio->idservicio],'files'=>'true'])!!}
            {{Form::token()}}
     <div class="row">
     	<div class="col-lg-6 col-md-6 col-xs-12">
     		<div class="form-group">
            	<label for="nombre">Nombre</label>
            	<input type="text" name="nombre" required value="{{$servicio->nombre}}" class="form-control">
            </div>
     	</div>
     	<div class="col-lg-6 col-md-6 col-xs-12">
     		<div class="form-group">
            	<label>Categoria</label>
            	<select name="idcategoria" class="form-control">
            		@foreach ($categorias as $cat)
            			@if ($cat->idcategoria==$servicio->idcategoria)
            			<option value="{{$cat->idcategoria}}" selected>{{$cat->nombre}}</option>
            			@else
            			<option value="{{$cat->idcategoria}}">{{$cat->nombre}}</option>
            			@endif
            		@endforeach
            	</select>
            </div>
     	</div>
     	<div class="col-lg-6 col-md-6 col-xs-12">
     		<div class="form-group">
            	<label for="codigo">Código</label>
            	<input type="text" name="codigo" required value="{{$servicio->codigo}}" class="form-control">
            </div>
     	</div>
     	<div class="col-lg-6 col-md-6 col-xs-12">
     		<div class="form-group">
            	<label for="stock">Stock</label>
            	<input type="text" name="stock" required value="{{$servicio->stock}}" class="form-control">
            </div>
     	</div>
     	<div class="col-lg-6 col-md-6 col-xs-12">
     		<div class="form-group">
            	<label for="descripcion">Descripción</label>
            	<input type="text" name="descripcion" value="{{$servicio->descripcion}}" class="form-control" placeholder="Descripcion del servicio...">
            </div>
     	</div>
     	<div class="col-lg-6 col-md-6 col-xs-12">
     		<div class="form-group">
            	<label for="imagen">Imagen</label>
            	<input type="file" name="imagen" class="form-control">
            	@if (($servicio->imagen)!="")
            		<img src="{{asset('imagenes/servicios/'.$servicio->imagen)}}" height="300px" width="300px">
            	@endif
            </div>
     	</div>
     	<div class="col-lg-6 col-md-6 col-xs-12">
     		<div class="form-group">
            	<button class="btn btn-primary" type="submit">Guardar</button>
            	<button class="btn btn-danger" type="reset">Cancelar</button> <!-- borra datos escritos -->
            </div>
     	</div>		
     </div>

			{!!Form::close()!!}		
            
		</div>
	</div>
@endsection