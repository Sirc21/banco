@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>Listado de Prestamos <a href="prestamo/create"><button class="btn btn-success">Nuevo</button></a></h3>
		@include('prestamos.prestamo.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Fecha</th>
					<th>Cliente</th>
					<th>Comprobante Tipo-Serie-Num</th> <!-- tipo serie numero-->
					<th>Impuesto</th>
					<th>Total</th>
					<th>Estado</th>
					<th>Opciones</th>
				</thead>
               @foreach ($prestamos as $pre)
				<tr>
					<td>{{ $pre->fecha_hora}}</td>
					<td>{{ $pre->nombre}}</td>
					<td>{{ $pre->tipo_comprobante.':'.$pre->serie_comprobante.'-'.$pre->num_comprobante}}</td>
					<td>{{ $pre->impuesto}}</td>
					<td>{{ $pre->total_prestamo}}</td>
					<td>{{ $pre->estado}}</td>
					<td>
						<a href="{{URL::action('PrestamoController@show',$pre->idprestamo)}}"><button class="btn btn-primary">Detalles</button></a>
                        <a href="" data-target="#modal-delete-{{$pre->idprestamo}}" data-toggle="modal"><button class="btn btn-danger">Anular</button></a>
						<a href="sdsdsdsd">sdsdsdsd</a>
                        <a href="{{URL::action('PrestamoController@show', $title = 'ver', $parameters = $pre->idprestamo), $attributes = ['class'=>'btn btn-primary']}}"></a>
					</td>
				</tr>
				@include('prestamos.prestamo.modal')
				@endforeach
			</table>
		</div>
		{{$prestamos->render()}}
	</div>
</div>

@endsection